package me.d3249.experiment.metaprog.annotations;

import com.squareup.javapoet.JavaFile;
import com.squareup.javapoet.MethodSpec;
import com.squareup.javapoet.TypeSpec;

import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.RoundEnvironment;
import javax.annotation.processing.SupportedAnnotationTypes;
import javax.annotation.processing.SupportedSourceVersion;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.TypeElement;
import javax.tools.Diagnostic;
import java.io.IOException;
import java.util.Set;

@SupportedSourceVersion(SourceVersion.RELEASE_8)
@SupportedAnnotationTypes({"me.d3249.experiment.metaprog.annotations.SayHi"})
public class SayHiProcessor extends AbstractProcessor {
    @Override
    public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnvironment) {

        for (TypeElement annotation : annotations) {
            roundEnvironment.getElementsAnnotatedWith(annotation).forEach(e -> {

                TypeElement classElement = (TypeElement) e;


                String fqn = classElement.getQualifiedName().toString();

                int lastDot = fqn.lastIndexOf('.');

                String packageName = lastDot > 0 ? fqn.substring(0, lastDot) : "";
                String extendedClassName = fqn.substring(lastDot + 1) + "Extended";

                MethodSpec hi = MethodSpec.methodBuilder("sayHi")
                        .addModifiers(Modifier.PUBLIC)
                        .returns(String.class)
                        .addStatement("return \"Hello World!\"")
                        .build();


                MethodSpec constructor = MethodSpec.constructorBuilder()
                        .addModifiers(Modifier.PUBLIC)
                        .addParameter(String.class, "name")
                        .addStatement("super(name)")
                        .build();

                TypeSpec newClass = TypeSpec.classBuilder(extendedClassName)
                        .superclass(classElement.asType())
                        .addModifiers(Modifier.PUBLIC, Modifier.FINAL)
                        .addMethod(constructor)
                        .addMethod(hi)
                        .build();

                try {
                    JavaFile.builder(packageName, newClass).build().writeTo(processingEnv.getFiler());
                } catch (IOException ioException) {
                    String message = String.format("Error extending class: %s", fqn);
                    processingEnv.getMessager().printMessage(Diagnostic.Kind.ERROR, message);
                    throw new AnnotationProcessorException(message);
                }

            });
        }


        return true;
    }
}

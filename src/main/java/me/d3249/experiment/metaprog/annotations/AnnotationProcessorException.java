package me.d3249.experiment.metaprog.annotations;

public class AnnotationProcessorException extends RuntimeException {
    public AnnotationProcessorException(String message) {
        super(message);
    }

    public AnnotationProcessorException(String message, Throwable cause) {
        super(message, cause);
    }
}

# Demo project

This POC for the use of a custom annotation to inspect a class and add a new method.

The code uses JavaPoet for code generation. When compiling the target it's needed to add JavaPoet jar to classpath.

##General description

At this point, the AnnotationProcessor retrieves the annotated class and extends it. Right now a constructor is generated
to call super. This is because the demo class used requires it. It's desirable to automatize constructor generation for use
in any custom class. Yet, that's out of the immediate scope of this POC.


##Demo target project

Demo target project is located [here](https://gitlab.com/d3249/annotation-processor-target)


## TODO

* Generate a fat Jar including JavaPoet
* Automatization of constructuctor definition
